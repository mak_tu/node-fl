const { createPool } = require('mysql'),
      { transliterate } = require('./text'),
      { queryAccess } = require('./db');

function enter(ctx) {
    try {
        console.dir(ctx.request.body, {colors: true});
        const pool = createPool(ctx.request.body);
        ctx.application.context.pool = pool;
        ctx.session = {access: true};
        ctx.redirect('/db.html');
    } catch (err) {
        ctx.redirect('/');
    }
};

function show(ctx) {
    const translit = transliterate(ctx.request.body.table).toLowerCase();
    console.log(translit);
    ctx.redirect(`${translit}-${ctx.request.body.radio}.html`);
};

function smetiAdd(ctx) {
    const body = ctx.request.body;
    const query = 'INSERT INTO smeti (??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
          array = ['code', 'id_item', 'name_item', 'count_created', 'id_material', 'code_material', 'name_material', 'consumption', 'count_material', 'cost', 'sum_teor', 'sum_fact', 'delta',
                    body.code, body.id_item, body.name_item, body.count_created, body.id_material, body.code_material, body.name_material, body.consumption, body.count_material, body.cost, body.sum_teor, body.sum_fact, body.delta]; 
    queryAccess(ctx, query, array);    
};

function zakaziAdd(ctx) {
    const body = ctx.request.body;
    const query = 'INSERT INTO zakazi (??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??, ??) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
          array = ['code', 'code_client', 'client', 'date', 'date_end', 'status', 'date_success', 'date_shipping', 'status_two', 'number_prod', 'name_prod', 'count', 'price', 'cost', 'total', 'code_prod',
                    body.code, body.code_client, body.client, body.date, body.date_end, body.status, body.date_success, body.date_shipping, body.status_two, body.number_prod, body.name_prod, body.count, body.price, body.cost, body.total, body.code_prod];
    queryAccess(ctx, query, array);                
};

function postavkiAdd(ctx) {
    const body = ctx.request.body;
    const query = '';
};

function potrebnostiAdd(ctx) {
    const body = ctx.request.body;
    let trashOne = [], trashTwo = [], trashThree = [];
    for (let i = 0; i < body.length; i++) { 
        trashOne[i] = '??';
        trashTwo[i] = '?';
    }

    for (let i in body) trashThree[i] = body[i];
    const query = `INSERT INTO potrebnosti (${trashOne.split(' ')}) VALUES(${trashTwo.split(' ')})`;
    //queryAccess(ctx, query, trashThree);
    console.dir(query, {colors: true});
    console.dir(trashThree, {colors: true});

}

exports.enter = enter;
exports.show = show;
exports.smetiAdd = smetiAdd;
exports.zakaziAdd = zakaziAdd;
exports.potrebnostiAdd = potrebnostiAdd;