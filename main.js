const Koa = require('koa'),
      static = require('koa-static'),
      route = require('koa-route'),
      session = require('koa-session'),
      bodyParser = require('koa-bodyparser'),
      req = require('./libs/req'); //callback'и для обработки AJAX  

const app = new Koa();

app.keys = ['some secret hurr'];
app.context.application = app;

const config = {
    key: 'koa:sess',
    maxAge: 86400000,
    overwrite: true,
    httpOnly: true,
    signed: true,
};

app.use(bodyParser());
app.use(session(config, app));

//СТАТИКА
app.use(static(__dirname + '/static/html'));
app.use(static(__dirname + '/static/css'));

//ЗАПРОСЫ
app.use(route.post('/enter', req.enter)); //подключение к БД
app.use(route.post('/show', req.show)); //главное меню
app.use(route.post('/smeti-add', req.smetiAdd)); //добавление данных в таблицу "Сметы"
app.use(route.post('/zakazi-add', req.zakaziAdd)); //добавление данных в таблицу "Заказы"
app.use(route.post('/postavki-add', req.postavkiAdd)); //добавление данных в таблицу "Поставки"
app.use(route.post('/potrebnosti-add', req.potrebnostiAdd)); //добавление данных в таблицу "Поставки"

app.listen(8000);