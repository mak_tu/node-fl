function createQuery(pool, query, array) {
    return new Promise((res, rej) => {
        pool.getConnection((err, connection) => {
            if (err) rej(err);
            connection.query(query, array, (err, results, fields) => {
                connection.release();
                if (err) rej(err);
                fields ? res(results) : res({results, fields});
            });
        });
    });
};

function queryAccess(ctx, query, array) {
    if (ctx.session.access) {
        try {
            createQuery(ctx.pool, query, array);                
        } catch (err) {
            throw err;
        }   
    } else {
        ctx.redirect('/');
    }
}

exports.queryAccess = queryAccess;